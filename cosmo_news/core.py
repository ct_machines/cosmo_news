import threading

from loguru import logger
from tqdm import tqdm

from cosmo_bot.core import bot
from news_puller.managers.instagram import InstagramManager
from news_puller.managers.vk import VKManager
from news_puller.models import Source
from news_puller.nlp.core import SpaCyInitializer


def check_messages():
    threading.Timer(10.0, check_messages).start()

    logger.info('Checking new messages')
    instagram = InstagramManager()
    vk = VKManager()
    nlp = SpaCyInitializer().nlp

    for source in tqdm(Source.objects.exclude(user=None)):
        logger.debug(source.url)
        try:
            manager = next(filter(lambda m: m.is_allowed(source.url), [instagram, vk]))
        except StopIteration:
            logger.error(f"Can't find manager for {source.url}")
        else:
            posts = manager.get_posts_and_update_last(source)
            users = source.user_set.all()
            for post in posts:
                doc = nlp(post.caption)
                was_categorized = any(probability > 0 for probability in doc.cats.values())
                for user in users:
                    if not was_categorized:
                        bot.send_message(user.chat_id, f'{post.caption}\n\n{post.url}')
                    else:
                        for category in user.subscribed_categories.all():
                            if doc.cats[category.name.lower()] > 0.4:
                                bot.send_message(user.chat_id, f'{post.caption}\n\n{post.url}')
                                break
