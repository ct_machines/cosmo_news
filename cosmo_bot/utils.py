from cosmo_bot.models import User

user = User()

TOKEN = '826531823:AAHTbmiyLz56sipv8peFjDWxqwnQ_g7Qsqw'

REQUEST_KWARGS = {
    'https': 'https://157.245.0.154:3128'
}

HELP_MSG = 'Write url what you want to subscribe. \n Also you can try this \n ' \
           '/social_networks to select filters by social networks \n' \
           '/content_type to select filters by categories'

BOT_URL = 't.me/cosmo_news_bot'

enum_for_social_networks = {
    'VKontakte': 'is_subscribed_to_vk',
    'Facebook': 'is_subscribed_to_facebook',
    'Instagram': 'is_subscribed_to_instagram'
}

enum_for_icon = {
    'ON': u'\u2714',
    'OFF': u'\u2716'
}

social_networks = [
    'VKontakte',
    'Facebook',
    'Instagram'
]

need_to_register_message = 'You need to register. Write /start '

permission_problem_message = 'You don\'t have permissions. Ask admin to solve it. Your id: '
