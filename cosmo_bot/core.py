import telebot
from telebot import apihelper, types
from vk_api import ApiError

from cosmo_bot.models import User, Category
from cosmo_bot.python_service import (
    get_str_without_u,
    make_markup_for_categories,
    make_string_with_icon,
    toggle_category,
    categories)
from cosmo_bot.utils import (
    BOT_URL,
    HELP_MSG,
    REQUEST_KWARGS,
    TOKEN,
    enum_for_social_networks,
    need_to_register_message,
    permission_problem_message,
    social_networks,
)
from news_puller.models import Source

bot = telebot.TeleBot(TOKEN)
apihelper.proxy = REQUEST_KWARGS
regex_for_url = '^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$'


@bot.message_handler(commands=['start'])
def start_session(session_info):
    user, created = User.objects.get_or_create(chat_id=session_info.chat.id)
    user.have_permission = True
    user.save()
    user.subscribed_categories.add(*Category.objects.all())
    if user.have_permission:
        bot.send_message(user.chat_id, '''
Привет!
Кидай мне ссылки на профили людей, о чьих обновлениях ты хочешь узнавать от меня:) 
Поддерживаются ссылки на открытые профили пользователей instagram и ВКонтакте.

Доступные команды:
/subscribe url — подписка на обновления пользователя
/unsubscribe url — отписка от пользователя
Помощь:
/help — список доступных команд
__________________
Фильтры:
/social_networks — выбор социальных сетей для сбора обновлений (по умолчанию все социальные сети выбраны)
/categories — выбор категорий, бот будет отправлять посты, соответствующие только выбранным тематикам (по умолчанию все категории выбраны)
''')
    else:
        bot.send_message(user.chat_id, permission_problem_message + str(user.chat_id))


@bot.message_handler(regexp=regex_for_url)
def add_url(message):
    try:
        user = User.objects.get(chat_id=message.chat.id)
        if user.have_permission:
            try:
                user.add_url(message.text)
                bot.send_message(user.chat_id, 'Succesfully added')
            except ApiError:
                bot.send_message(user.chat_id, 'Invalid url')
        else:
            bot.send_message(user.chat_id, permission_problem_message + str(user.chat_id))
    except User.DoesNotExist:
        bot.send_message(message.chat.id, need_to_register_message)


@bot.message_handler(commands=['help'])
def send_instruction(message):
    user, created = User.objects.get_or_create(chat_id=message.chat.id)
    bot.send_message(user.chat_id, HELP_MSG)


@bot.message_handler(commands=['upgrade_user_permission'])
def upgrade_user(message):
    try:
        user = User.objects.get(chat_id=message.chat.id)
        if user.is_admin:
            chat_id = str(message.text).split()[1]
            try:
                user_upgrade = User.objects.get(chat_id=chat_id)
                user_upgrade.have_permission = True
                user_upgrade.save()
            except User.DoesNotExist:
                bot.send_message(user.chat_id, 'This user isn\'t registered yet')

        else:
            bot.send_message(user.chat_id, permission_problem_message + str(user.chat_id))
    except User.DoesNotExist:
        bot.send_message(message.chat.id, need_to_register_message)


@bot.message_handler(commands=['get_bot_url'])
def send_bot_url(message):
    try:
        user = User.objects.get(chat_id=message.chat.id)
        bot.send_message(user.chat_id, BOT_URL)
    except User.DoesNotExist:
        bot.send_message(message.chat.id, need_to_register_message)


@bot.message_handler(commands=['unsubscribe'])
def unsubscribe(message):
    try:
        user = User.objects.get(chat_id=message.chat.id)
        if user.have_permission:
            url = message.text.split()[1]
            try:
                source = Source.objects.get(url=url)
                user.unsubscribe(source)
                bot.send_message(user.chat_id, 'Successfully unsubscribed')
            except Source.DoesNotExist:
                bot.send_message(user.chat_id, 'You didn\'t subscribe to this url')
        else:
            bot.send_message(user.chat_id, permission_problem_message)
    except User.DoesNotExist:
        bot.send_message(message.chat.id, need_to_register_message)


@bot.message_handler(commands=['social_networks'])
def get_markup_social_networks(message):
    try:
        user = User.objects.get(chat_id=message.chat.id)
        if user.have_permission:
            markup = types.ReplyKeyboardMarkup()
            markup.row(make_string_with_icon(' Instagram', getattr(user, enum_for_social_networks['Instagram'])))
            markup.row(make_string_with_icon(' VKontakte', getattr(user, enum_for_social_networks['VKontakte'])))
            markup.row(make_string_with_icon(' Facebook', getattr(user, enum_for_social_networks['Facebook'])))
            bot.send_message(user.chat_id, "Choose social network:", reply_markup=markup)
        else:
            bot.send_message(user.chat_id, permission_problem_message + str(user.chat_id))
    except User.DoesNotExist:
        bot.send_message(message.chat.id, need_to_register_message)


@bot.message_handler(commands=['categories'])
def get_markup_content_types(message):
    try:
        user = User.objects.get(chat_id=message.chat.id)
        if user.have_permission:
            markup = make_markup_for_categories(user)
            bot.send_message(user.chat_id, "Choose content type:", reply_markup=markup)
        else:
            bot.send_message(user.chat_id, permission_problem_message + str(user.chat_id))
    except User.DoesNotExist:
        bot.send_message(message.chat.id, need_to_register_message)


@bot.message_handler(commands=['get_subscriptions'])
def get_subscriptions(message):
    try:
        user = User.objects.get(chat_id=message.chat.id)
        if user.have_permission:
            sources = user.sources.all()
            if len(sources):
                subscribed_sources = ', '.join([str(x.url) for x in sources])
                bot.send_message(user.chat_id, subscribed_sources)
            else:
                bot.send_message(user.chat_id, 'No subscriptions')
        else:
            bot.send_message(user.chat_id, permission_problem_message + str(user.chat_id))
    except User.DoesNotExist:
        bot.send_message(message.chat.id, need_to_register_message)


@bot.message_handler(func=lambda message: get_str_without_u(message.text) in categories)
def toggle_filter_categories(message):
    try:
        user = User.objects.get(chat_id=message.chat.id)
        if user.have_permission:
            category = get_str_without_u(message.text)
            is_toggled_on = toggle_category(category=category, user=user)
            markup = make_markup_for_categories(user)
            bot.send_message(user.chat_id, category + ' toggled ' +
                             ('ON' if is_toggled_on else 'OFF'),
                             reply_markup=markup)
        else:
            bot.send_message(user.chat_id, permission_problem_message + str(user.chat_id))
    except User.DoesNotExist:
        bot.send_message(message.chat.id, need_to_register_message)


@bot.message_handler(func=lambda message: get_str_without_u(message.text) in social_networks)
def toggle_filter_social_networks(message):
    try:
        category = get_str_without_u(message.text)
        user = User.objects.get(chat_id=message.chat.id)
        if user.have_permission:
            user.add_category_social_network(category=category)
            bot.send_message(user.chat_id, category + ' toggled ' + ('ON'
                                                                     if getattr(user, enum_for_social_networks[
                category]) else 'OFF'),
                             reply_markup=types.ReplyKeyboardRemove())
        else:
            bot.send_message(user.chat_id, permission_problem_message + str(user.chat_id))
    except User.DoesNotExist:
        bot.send_message(message.chat.id, need_to_register_message)


@bot.message_handler(func=lambda m: True)
def echo_all(message):
    try:
        user = User.objects.get(chat_id=message.chat.id)
        if user.have_permission:
            bot.send_message(user.chat_id, message.text, parse_mode='Markdown')
        else:
            bot.send_message(user.chat_id, permission_problem_message + str(user.chat_id))
    except User.DoesNotExist:
        bot.send_message(message.chat.id, need_to_register_message)


def main():
    try:
        bot.polling(none_stop=True, timeout=123)
    except:
        print('Something went wrong. Reboot')


if __name__ == '__main__':
    main()
