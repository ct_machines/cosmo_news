from django.contrib.postgres.fields import ArrayField
from django.db import models

# Create your models here.
from news_puller.models import Source


class Category(models.Model):
    name = models.CharField(max_length=100, default='')


class User(models.Model):
    chat_id = models.IntegerField(default=0)
    user_id = models.IntegerField(default=0)
    first_name = models.CharField(max_length=100, default='')
    last_name = models.CharField(max_length=100, default='')
    username = models.CharField(max_length=100, default='')
    language_code = models.CharField(max_length=100, default='')
    sources = models.ManyToManyField(Source)
    is_subscribed_to_vk = models.BooleanField(default=False)
    is_subscribed_to_facebook = models.BooleanField(default=False)
    is_subscribed_to_instagram = models.BooleanField(default=False)
    subscribed_categories = models.ManyToManyField(Category)
    is_admin = models.BooleanField(default=False)
    have_permission = models.BooleanField(default=False)

    def add_url(self, url):
        source, created = Source.objects.get_or_create(url=url)
        print(source.id)
        self.sources.add(source)
        User.objects.get_or_create(chat_id=self.chat_id, username=self.username)

    def add_category_social_network(self, category):
        if category == 'Instagram':
            self.is_subscribed_to_instagram = not self.is_subscribed_to_instagram
        if category == 'VKontakte':
            self.is_subscribed_to_vk = not self.is_subscribed_to_vk
        if category == 'Facebook':
            self.is_subscribed_to_facebook = not self.is_subscribed_to_facebook
        self.save()

    def add_category_content_type(self, category):
        category, created = Category.objects.get_or_create(name=category)
        self.subscribed_categories.add(category)

    def unsubscribe(self, source):
        self.sources.remove(source)

    def __str__(self):
        return self.username
