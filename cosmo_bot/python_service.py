from telebot import types

from cosmo_bot.models import Category
from cosmo_bot.utils import enum_for_icon


def get_str_without_u(string):
    return string.replace(u'\u2714 ', '').replace(u'\u2716 ', '')


def make_string_with_icon(string, flag):
    return (enum_for_icon['OFF'] if flag else enum_for_icon['ON']) + string


def make_string_for_category(category, user):
    return make_string_with_icon(category, is_category_exist(category, user))


def make_markup_for_categories(user):
    markup = types.ReplyKeyboardMarkup()
    row = []
    for i, category in enumerate(categories):
        row.append(make_string_for_category(category=' ' + category, user=user))
        if i % 2:
            markup.row(*row)
            row = []
    if row:
        markup.row(*row)
    return markup


def is_category_exist(category, user):
    category = category.replace(' ', '')
    try:
        user.subscribed_categories.get(name=category)
        return True
    except Category.DoesNotExist:
        return False


def toggle_category(category, user):
    flag = is_category_exist(category, user)
    if flag:
        matching_categories = user.subscribed_categories.get(name=category)
        user.subscribed_categories.remove(matching_categories)
    else:
        user.add_category_content_type(category=category)
    return not flag


categories = [category.name for category in Category.objects.all()]
