from pathlib import Path

import cv2

base_dir = Path(__file__).parent


def has_cad(cat_path):
    image = cv2.imread(cat_path)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    for haarascade in ['haarcascade_frontalcatface.xml', 'haarcascade_frontalcatface_extended.xml']:
        detector = cv2.CascadeClassifier(str(base_dir / haarascade))
        rects = detector.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=10, minSize=(75, 75))
        if type(rects) != tuple:
            return True

    return False
