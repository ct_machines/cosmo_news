from abc import ABC, abstractmethod


class Post:
    def __init__(self, url, text, post_id):
        self.url = url
        self.caption = text
        self.id = post_id


class Manager(ABC):
    @property
    @abstractmethod
    def base(self):
        pass

    @classmethod
    def is_allowed(cls, url):
        return cls.base in url

    def get_posts_and_update_last(self, source, count=3):
        posts = list(map(self.parse_post, self.get_data(source, count)))

        last_id = source.last_post_id
        for post in posts.copy():
            if post.id <= source.last_post_id:
                posts.remove(post)
            last_id = max(last_id, post.id)
        source.last_post_id = last_id
        source.save()

        return posts

    @abstractmethod
    def get_data(self, source, count):
        pass

    @staticmethod
    @abstractmethod
    def parse_post(data):
        pass

    @staticmethod
    @abstractmethod
    def get_profile_id(url: str):
        pass
