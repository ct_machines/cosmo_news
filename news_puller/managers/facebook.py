import re

import facebook
import requests
from django.conf import settings

from news_puller.managers.common import Manager, Post


class FacebookManager(Manager):
    base = 'facebook.com'

    def __init__(self):
        self.graph = facebook.GraphAPI(access_token=settings.FB_ACCESS_TOKEN, version='2.12')

    def get_data(self, source, count):
        response = self.graph.get_connections(id='me', connection_name='friends')
        return response['data']

    @staticmethod
    def parse_post(data):
        return Post(url='none', text='none', post_id='none')

    def get_profile_id(self, url: str):
        id_re = re.compile('"entity_id":"([0-9]+)"')
        page = requests.get(url)
        page_id = id_re.findall(page.content.decode())[0]
        return page_id
