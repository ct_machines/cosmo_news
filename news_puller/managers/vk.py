import vk_api
from django.conf import settings

from news_puller.managers.common import Manager, Post


class VKManager(Manager):
    base = 'vk.com'

    def __init__(self):
        session = vk_api.VkApi(settings.VK_LOGIN, settings.VK_PASSWORD)
        session.auth()
        self.vk = session.get_api()

    def get_data(self, source, count):
        data = self.vk.wall.get(owner_id=source.profile_id, count=count)
        return data['items']

    @classmethod
    def parse_post(cls, data):
        owner_id = data['owner_id']
        post_id = data['id']
        return Post(
            url=f'https://{cls.base}/wall{owner_id}_{post_id}',
            text=data['text'],
            post_id=post_id,
        )

    def get_profile_id(self, url: str):
        user_name = url.split('/')[-1]
        profile_id = self.vk.users.get(user_ids=[user_name])[0]['id']
        return profile_id
