import hashlib

import requests
from instagram_web_api import Client

from news_puller.managers.common import Manager, Post


class FixedClient(Client):
    @staticmethod
    def _extract_rhx_gis(html):
        return hashlib.md5(b'tmp_str')


class InstagramManager(Manager):
    base = 'instagram.com'

    def __init__(self):
        self.web_api = FixedClient(auto_patch=True, drop_incompat_keys=False)

    def get_data(self, source, count):
        return self.web_api.user_feed(source.profile_id, count=count)

    @staticmethod
    def parse_post(data):
        node = data['node']
        node_id = int(node['id'].split('_')[0])
        return Post(
            url=node['link'],
            text=node['caption']['text'],
            post_id=node_id,
        )

    @staticmethod
    def get_profile_id(url: str):
        response = requests.get(url, params={'__a': 1}).json()
        return response['graphql']['user']['id']
