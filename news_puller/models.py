from django.db import models
from loguru import logger

from news_puller.managers.instagram import InstagramManager
from news_puller.managers.vk import VKManager


class Source(models.Model):
    url = models.URLField(unique=True)
    profile_id = models.CharField(max_length=64, blank=True)
    last_post_id = models.DecimalField(max_digits=25, decimal_places=0, default='0')

    def save(self, **kwargs):
        if not self.profile_id:
            try:
                manager_class = next(filter(lambda m: m.is_allowed(self.url), [
                    InstagramManager,
                    VKManager,
                ]))
            except StopIteration:
                logger.error(f"Can't find manager for {self.url}")
            else:
                manager = manager_class()
                self.profile_id = manager.get_profile_id(self.url)
        super().save(**kwargs)
