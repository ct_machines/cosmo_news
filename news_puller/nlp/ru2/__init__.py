# encoding: utf8
from __future__ import print_function, unicode_literals

from spacy.lang.ru import Russian, RussianDefaults

from ru2.lemmatizer import RussianLemmatizer


class Russian2Defaults(RussianDefaults):
    @classmethod
    def create_lemmatizer(cls, nlp=None):
        return RussianLemmatizer()


class Russian2(Russian):
    lang = 'ru'
    Defaults = Russian2Defaults


__all__ = ['Russian2']
