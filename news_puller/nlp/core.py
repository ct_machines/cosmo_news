from enum import Enum
from pathlib import Path

import spacy
from loguru import logger


class SpaCyInitializer:
    base_dir = Path(__file__).parent
    nlp_model_dir: Path = base_dir / 'ru2'

    def __init__(self, force_blank=False):
        if force_blank or not self.nlp_model_dir.exists():
            self.nlp = spacy.blank('ru')
            logger.error('NLP model is empty. You should train it.')
        else:
            self.nlp = spacy.load(self.nlp_model_dir)


class Categories(Enum):
    beauty = 'beauty'
    children = 'children'
    relationship = 'relationship'
    sport = 'sport'
    music = 'music'
    film = 'film'
    art = 'art'
    travel = 'travel'
    animals = 'animals'
    politics = 'politics'
