import random

import pandas as pd
from loguru import logger
from spacy.util import compounding, minibatch
from tqdm import trange

from news_puller.nlp.core import Categories, SpaCyInitializer


class SpaCyTrainer(SpaCyInitializer):
    def train(self, categories_data_path, text_key='text', n_iter=100):
        train_data = self.get_train_data_from_csv(categories_data_path, text_key)
        if 'textcat' in self.nlp.pipe_names:
            textcat = self.nlp.get_pipe('textcat')
        else:
            textcat = self.nlp.create_pipe('textcat')
            self.nlp.add_pipe(textcat, last=True)

        for category in Categories:
            textcat.add_label(category.value)

        other_pipes = [pipe for pipe in self.nlp.pipe_names if pipe != 'textcat']
        with self.nlp.disable_pipes(*other_pipes):
            optimizer = self.nlp.begin_training()
            for _ in trange(n_iter):
                losses = {}
                random.shuffle(train_data)
                batches = minibatch(train_data, size=compounding(4., 64., 1.001))
                for batch in batches:
                    texts, annotations = zip(*batch)
                    self.nlp.update(texts, annotations, sgd=optimizer, drop=0.2, losses=losses)
        logger.info('Writing to disk')
        with self.nlp.use_params(optimizer.averages):
            self.nlp.to_disk(self.nlp_model_dir)

    def get_train_data_from_csv(self, file_path, text_key):
        result = []
        data = pd.read_csv(file_path, keep_default_na=False)

        for i, row in data.iterrows():
            text = row[text_key]

            doc = self.nlp(text)
            tokens = []
            for token in doc:
                if not token.is_stop:
                    tokens.append(token.text)

            cats = {}
            for category in Categories:
                cat_name = category.value
                cats[cat_name] = bool(row[cat_name])
            result.append((' '.join(tokens), {'cats': cats}))

        return result


if __name__ == '__main__':
    trainer = SpaCyTrainer()
    trainer.train(trainer.base_dir / 'train_data' / 'categories.csv')
