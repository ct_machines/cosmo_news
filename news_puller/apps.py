from django.apps import AppConfig


class NewsPullerConfig(AppConfig):
    name = 'news_puller'
